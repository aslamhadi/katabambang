import os
import random
from telegram.ext import Updater, CommandHandler


TOKEN = os.environ.get("TELEGRAM_TOKEN")
quotes = [
    "Mau nyelimutin, ga ada selimutnya... Fotoin aja dah, kan ada kameranya...",
    "Mudah-mudahan yang menang nggak kalah..."
]


def get_quote(bot, update):
    idx = random.randint(0, 1)
    update.message.reply_text(quotes[idx])


updater = Updater(TOKEN)
updater.dispatcher.add_handler(CommandHandler('bambang', get_quote))

updater.start_polling()
updater.idle()
